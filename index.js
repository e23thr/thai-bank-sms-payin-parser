const parserList = {
  kbank: {
    th: {
      match: /บ/,
      date: /([0-9]{2}\/[0-9]{2}\/[0-9]{2}\s[0-9]{2}:[0-9]{2})/,
      destAcc: /บช(X[0-9]{6}X)/,
      amount: /จาก.+\s([0-9,.]+)/
    }
  },
  ktb: {
    th: {
      match: /บ/,
      date: /([0-9]{2}-[0-9]{2}-[0-9]{2}@[0-9]{2}:[0-9]{2})/,
      destAcc: /บช([0-9Xx]+)/,
      amount: /\+([0-9,.]+)/
    }
  }
};

function getRightParser(text) {
  let result = null;
  Object.keys(parserList).forEach(bank => {
    Object.keys(parserList[bank]).forEach(subtype => {
      const sub = parserList[bank][subtype];
      if (
        sub.date.test(text) &&
        sub.destAcc.test(text) &&
        sub.amount.test(text)
      )
        result = { type: bank, subtype: subtype, parser: sub };
    });
  });
  return result;
}

function parseBankSMS(text) {
  const parser = getRightParser(text);
  if (parser) {
    return {
      _bank: parser.type,
      _bank_subtype: parser.subtype,
      date: text.match(parser.parser.date)[1],
      destAcc: text.match(parser.parser.destAcc)[1],
      amount: text.match(parser.parser.amount)[1]
    };
  }
  return null;
}

module.exports = parseBankSMS;
