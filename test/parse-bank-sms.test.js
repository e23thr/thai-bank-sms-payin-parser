const { expect } = require("chai");
const fs = require("fs");
const parseBankSMS = require("../index");

const data_dir = "./sms-samples";

const fileList = fs.readdirSync(`${data_dir}`).filter(f => f.endsWith(".json"));

fileList.forEach(file => {
  const content = JSON.parse(fs.readFileSync(`${data_dir}/${file}`));
  describe("Content", () => {
    it("must be an object", () => {
      expect(content).to.be.an("object");
    });
  });

  describe(`parseBankSMS( ${content.message} )`, () => {
    const result = parseBankSMS(content.message);
    // console.log('result',result);
    it(`should not return null for ${content.sender}`, () => {
      expect(result).to.not.be.null;
    });
    it(`should have return as object`, () => {
      expect(result).to.be.an("object");
    });
    it(`should have date`, () => {
      expect(result.date).to.not.be.empty;
    });
    it("should have destAcc", () => {
      expect(result.destAcc).to.not.be.empty;
    });
    it("should have amount", () => {
      expect(result.amount).to.not.be.empty;
    });
  });
});
