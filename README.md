## thai-bank-sms-payin-master
พัฒนามาเพื่อใช้แปลงข้อความ SMS แจ้งการรับโอนเงินจากธนาคารต่าง ออกมาเป็น object เพื่อให้สะดวกในการใช้งาน

## ธนาคารที่ใช้ได้
* กสิกรไทย (ภาษาไทย)
* กรุงไทย (ภาษาไทย)

## การช่วยเหลือ
เนื่องจากผมไม่สามารถรับ SMS แจ้งโอนเงินได้ทุกธนาคาร คุณสามารถช่วยเหลือโดยการให้ข้อมูลเพิ่มเติมได้ดังนี้
* แคปภาพ SMS ของธนาคาร ที่ module ยังไม่ support มาให้ เพื่อจะได้นำมาเพิ่มเติมต่อไป
* แจ้งปัญหาการแปลงข้อความไม่ถูกต้องโดยส่งภาพหน้าจอข้อความที่ทำงานไม่ถูกต้อง

กรุณาส่งภาพแคปหน้าจอมาที่เมลผม [praphan.o@gmail.com](mailto:praphan.o@gmail.com) หรือ Add line ผมก็ยิ่งดีเลยครับ Line ID: [orpl918](https://line.me/ti/p/pQXOE9ywC7)

## Installation
```
npm install --save thai-bank-sms-payin-parser
```

## Documentation
```
const parseBankSMS = require('thai-bank-sms-payin-parser');
const parsed = parseBankSMS("SMS MESSAGE HERE");
console.log(parsed);
```
